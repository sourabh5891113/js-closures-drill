function limitFunctionCallCount(cb, n) {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
  if (typeof cb === "function" && typeof n === "number") {
    let count = 0;
    return () => {
      if (count < n) {
        cb();
        count++;
      } else {
        console.log("callback exhausted can't invoke anymore");
        return null;
      }
    };
  } else {
    console.log(
      "First argument should be a function and second should be a number"
    );
    return null;
  }
}

function cb() {
  console.log("callback invoked");
}

module.exports = { limitFunctionCallCount, cb };
