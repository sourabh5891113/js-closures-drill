function cacheFunction(cb) {
  // Should return a function that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.
  let cacheObject = {};
  return (...args) => {
    let key = JSON.stringify([...args]);
    // console.log(cacheObject);
    if (cacheObject[key] !== undefined) {
      console.log("Catched value is " + cacheObject[key]);
    } else {
      let result = cb(...args);
      cacheObject = { ...cacheObject, [key]: result };
    }
  };
}
function cb(arg1, arg2, arg3) {
  let args = [...arguments];
  let result = Math.round(Math.random() * 100);
  console.log("callback invoked with " + args + " result is " + result);
  return result;
}

module.exports = { cacheFunction, cb };
