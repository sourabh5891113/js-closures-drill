function counterFactory() {
  // Return an object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
  let counter = 0;
  return {
    increment(n) {
      if (n === undefined) {
        n = 1;
      }
      console.log(`Incresing counter by ${n} => Counter = ${(counter += n)}`);
    },
    decrement(n) {
      if (n === undefined) {
        n = 1;
      }
      console.log(`Decreasing counter by ${n} => Counter = ${(counter -= n)}`);
    },
  };
}

module.exports = counterFactory;
