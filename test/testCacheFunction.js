let { cacheFunction, cb } = require("../cacheFunction");

let higherOrderFunction = cacheFunction(cb);

higherOrderFunction(1, 2, 3); //called first time
higherOrderFunction(1, 2, 3); //returned from catch object
higherOrderFunction("sourabh", "thakur"); //called first time
higherOrderFunction("sourabh", "thakur"); //returned from catch object

