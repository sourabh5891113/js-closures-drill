let { limitFunctionCallCount, cb } = require("../limitFunctionCallCount");

try {
  let limitedFunction = limitFunctionCallCount(cb, 2);
  limitedFunction();
  limitedFunction();
  limitedFunction();
  limitedFunction();
} catch (err) {
  console.error("Error => " + err);
}
