let counterFactory = require("../counterFactory");

let counterObject = counterFactory();
counterObject.increment(); //1
counterObject.increment(2); //3
counterObject.increment(3); //6
counterObject.decrement(); //5
counterObject.decrement(3); //2
